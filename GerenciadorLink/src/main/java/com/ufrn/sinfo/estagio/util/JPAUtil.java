package com.ufrn.sinfo.estagio.util;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.ufrn.sinfo.estagio.dao.LinkDao;
import com.ufrn.sinfo.estagio.dominio.Link;

public class JPAUtil {
	private static EntityManagerFactory factory;
	
	static {
		factory = 
				Persistence.createEntityManagerFactory("hibernate_jpa_db");
	}
	
	public static EntityManager createEntityManger() {
		return factory.createEntityManager();
	}
	
	public static void closeFactory() {
		factory.close();
	}
	
	public static void main(String... args) {
		new JPAUtil();
		LinkDao dao = new LinkDao();
		Link link = new Link();
		link.setNome("google.com");
		dao.adicionar(link);
	}
}